<?php
/** 
* Functions related to ACF
*/

// Add options pages
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));
	
}



function themeprefix_acf_global_settings_context( $context ) {

  // make ACF options page fields available in every Twig file
  $context['options'] = get_fields( 'option' );

  return $context;
}

add_filter( 'timber_context', 'themeprefix_acf_global_settings_context' );



// Wraps field definitions in __() function when exporting to PHP via ACF admin UI
function themeprefix_acf_localize_fields_when_exporting() {
  acf_update_setting( 'l10n_textdomain', 'theme_domain' );
}

add_action( 'acf/init', 'themeprefix_acf_localize_fields_when_exporting' );



// Disable "Custom fields" menu in admin
// You need to load environment to WP in order for this to work
// Eg. by adding autoload.php from a site' vendor folder to wp-config.php
/* add_filter(
  'acf/settings/show_admin', 
  function() {
    return getenv( 'ENV' ) === 'dev' ? true : false;
  }
);
 */

 /*** Remove Default Post Type ***/
 
//Remove from Quick Draft
add_action( 'wp_dashboard_setup', 'remove_draft_widget', 999 );
 
function remove_draft_widget(){
    remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
}
//Remove from +New Post in Admin Bar
add_action( 'admin_bar_menu', 'remove_default_post_type_menu_bar', 999 );
 
function remove_default_post_type_menu_bar( $wp_admin_bar ) {
    $wp_admin_bar->remove_node( 'new-post' );
}
 
//Remove from the Side Menu
add_action( 'admin_menu', 'remove_default_post_type' );
 
function remove_default_post_type() {
    remove_menu_page( 'edit.php' );
}
