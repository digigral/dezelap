<?php
/**
* Functions (pre_get_posts for example) that refer to specific templates
*  You can add more files like this for other templates in functions/templates/<template-name>.php
*  and add a require statement in the functions.php to include them
*/

function wpd_testimonials_query( $query ){
    if( ! is_admin()){
        $query->set( 'posts_per_page', 50 );
    }
}
add_action( 'pre_get_posts', 'wpd_testimonials_query' );