<?php
/** 
* Registering scripts and styles
*/

function themeprefix_script_enqueuer() {
  $scriptdeps_site = [];
 
  wp_register_script( 'mediaelement-and-player', 'https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.2.6/mediaelement-and-player.min.js', $scriptdeps_site, false, true );
 
  wp_register_script( 'skip-back', 'https://cdnjs.cloudflare.com/ajax/libs/mediaelement-plugins/2.5.0/skip-back/skip-back.min.js', $scriptdeps_site, false, true );
 
  wp_register_script( 'jump-forward', 'https://cdnjs.cloudflare.com/ajax/libs/mediaelement-plugins/2.5.0/jump-forward/jump-forward.min.js', $scriptdeps_site, false, true );

  wp_register_script( 'changespeed',  'https://cdn.jsdelivr.net/gh/ivorpad/mediaelement-changespeed/changespeed.js', $scriptdeps_site, false, true );

  wp_register_script( 'slick-js',  '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', $scriptdeps_site, false, true );

  if ( is_single() && 'pravljice' == get_post_type() ) {
    wp_enqueue_script( 'mediaelement-and-player' );
    wp_enqueue_script( 'skip-back' );
    wp_enqueue_script( 'jump-forward' );
    wp_enqueue_script( 'changespeed' );
    wp_enqueue_script( 'slick-js' );
  }

  wp_register_script( 'site', get_template_directory_uri() . '/js/scripts.min.js', array('jquery'), false, true );
  wp_enqueue_script( 'site' );

  wp_localize_script( 'site', 'localized_strings', themeprefix_localized_strings() );
  wp_localize_script( 'site', 'script_data', themeprefix_script_data() );

  wp_register_style( 'mediaelementplayer', 'https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.2.6/mediaelementplayer.css', '', '', 'screen' );
  
  wp_register_style( 'speed', 'https://cdnjs.cloudflare.com/ajax/libs/mediaelement-plugins/2.5.0/speed/speed.min.css', '', '', 'screen' );

  wp_register_style( 'skip-back',  'https://cdnjs.cloudflare.com/ajax/libs/mediaelement-plugins/2.5.0/skip-back/skip-back.min.css', '', '', 'screen' );

  wp_register_style( 'jump-forward', 'https://cdnjs.cloudflare.com/ajax/libs/mediaelement-plugins/2.5.0/jump-forward/jump-forward.min.css', '', '', 'screen' );

  wp_register_style( 'slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css', '', '', 'screen' );

  if ( is_single() && 'pravljice' == get_post_type() ) {
    wp_enqueue_style( 'mediaelementplayer' );
    wp_enqueue_style( 'speed' );
    wp_enqueue_style( 'skip-back' );
    wp_enqueue_style( 'jump-forward' );
    wp_enqueue_style( 'slick' );
  }

  wp_register_style( 'screen', get_stylesheet_directory_uri() . '/style.css', '', '', 'screen' );
  wp_enqueue_style( 'screen' );
}

add_action( 'wp_enqueue_scripts', 'themeprefix_script_enqueuer', 10 );



function themeprefix_defer_scripts( $tag, $handle, $src ) {

  // The handles of the enqueued scripts we want to defer
  $defer_scripts = [];

  if ( in_array( $handle, $defer_scripts ) ) {
    return '<script src="' . $src . '" defer="defer" type="text/javascript"></script>\n';
  }
  
  return $tag;
}

add_filter( 'script_loader_tag', 'themeprefix_defer_scripts', 10, 3 );
