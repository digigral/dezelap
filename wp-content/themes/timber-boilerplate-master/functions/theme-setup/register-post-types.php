<?php
/**
* Registering various content types
*/

function themeprefix_register_post_types() {
  register_post_type( 'pravljice', array(
    'labels'             => array(
      'name'               => __( 'Pravljice', 'saleszone' ),
      'singular_name'      => __( 'Pravljica', 'saleszone' ),
      'add_new'            => __( 'Add a new', 'saleszone' ),
      'add_new_item'       => __( 'Add Pravljica', 'saleszone' ),
      'edit_item'          => __( 'Edit Pravljica', 'saleszone' ),
      'new_item'           => __( 'New Pravljica', 'saleszone' ),
      'view_item'          => __( 'View Pravljica', 'saleszone' ),
      'search_items'       => __( 'Search for Pravljica', 'saleszone' ),
      'not_found'          => __( 'Pravljica not found', 'saleszone' ),
      'not_found_in_trash' => __( 'No pravljice found in the trash', 'saleszone' ),
      'all_items'          => __( 'Vse pravljice', 'saleszone' ),
      'archives'           => __( 'Archives of pravljice', 'saleszone' ),
      'insert_into_item'   => __( 'Paste into Pravljica', 'saleszone' ),
      'menu_name'          => __( 'Pravljice', 'saleszone' ),
      'items_list'         => __( 'Pravljica list', 'saleszone' ),
    ),
    'public'             => true,
    'publicly_queryable' => true,
    'menu_position'      => null,
    'show ui'            => true,
    'menu_icon'          => 'dashicons-format-aside',
    'capability_type'    => 'post',
    'hierarchical'       => true,
    'taxonomies'         => array( 'vrsta-pravljice'),
    'has_archive'        => true,
    'rewrite'            => array( 'slug' => 'pravljice' ),
    'query_var'          => true,
    'show_in_rest'		 => true,
    'supports'           => array(
      'title',
      'editor',
      'thumbnail',
      'excerpt',
      'trackbacks',
      'custom-fields',
      'comments',
      'revisions',
      'page-attributes',
    ),
  ) );
  register_taxonomy(
    'avtor-pravljice',
    'pravljice',
    array(
      'labels'       => array(
        'name'          => __( 'Avtor pravljice', 'saleszone' ),
        'singular_name' => __( 'Avtor pravljice', 'saleszone' ),
        'menu_name'     => __( 'Avtor pravljice', 'saleszone' ),
        'all_items'     => __( 'All Avtor pravljice', 'saleszone' ),
        'edit_item'     => __( 'Change Avtor pravljice', 'saleszone' ),
        'view_item'     => __( 'View Avtor pravljice', 'saleszone' ),
        'update_item'   => __( 'Update Avtor pravljice', 'saleszone' ),
        'add_new_item'  => __( 'Add a new Avtor pravljice to the gallery', 'saleszone' ),
        'search_items'  => __( 'Search Avtor pravljice', 'saleszone' ),
        'not_found'     => __( 'No Avtor pravljice found.', 'saleszone' ),
      ),
      'hierarchical'      => true,
      'show_in_rest' =>true,
      'rewrite' => array(
        'slug' => 'avtor-pravljice'
      ),
    )
  );
  register_taxonomy(
    'regija-pravljice',
    'pravljice',
    array(
      'labels'       => array(
        'name'          => __( 'Regija pravljice', 'saleszone' ),
        'singular_name' => __( 'Regija pravljice', 'saleszone' ),
        'menu_name'     => __( 'Regija pravljice', 'saleszone' ),
        'all_items'     => __( 'All Regija pravljice', 'saleszone' ),
        'edit_item'     => __( 'Change Regija pravljice', 'saleszone' ),
        'view_item'     => __( 'View Regija pravljice', 'saleszone' ),
        'update_item'   => __( 'Update Regija pravljice', 'saleszone' ),
        'add_new_item'  => __( 'Add a new Regija pravljice to the gallery', 'saleszone' ),
        'search_items'  => __( 'Search Regija pravljice', 'saleszone' ),
        'not_found'     => __( 'No Regija pravljice found.', 'saleszone' ),
      ),
      'hierarchical'      => true,
      'show_in_rest' =>true,
      'rewrite' => array(
        'slug' => 'regija-pravljice'
      ),
    )
  );
  register_taxonomy(
    'starost-pravljice',
    'pravljice',
    array(
      'labels'       => array(
        'name'          => __( 'Starost pravljice', 'saleszone' ),
        'singular_name' => __( 'Starost pravljice', 'saleszone' ),
        'menu_name'     => __( 'Starost pravljice', 'saleszone' ),
        'all_items'     => __( 'All Starost pravljice', 'saleszone' ),
        'edit_item'     => __( 'Change Starost pravljice', 'saleszone' ),
        'view_item'     => __( 'View Starost pravljice', 'saleszone' ),
        'update_item'   => __( 'Update Starost pravljice', 'saleszone' ),
        'add_new_item'  => __( 'Add a new Starost pravljice to the gallery', 'saleszone' ),
        'search_items'  => __( 'Search Starost pravljice', 'saleszone' ),
        'not_found'     => __( 'No Starost pravljice found.', 'saleszone' ),
      ),
      'hierarchical'      => true,
      'show_in_rest' =>true,
      'rewrite' => array(
        'slug' => 'starost-pravljice'
      ),
    )
  );
  register_taxonomy(
    'kategorija-pravljice',
    'pravljice',
    array(
      'labels'       => array(
        'name'          => __( 'kategorija pravljice', 'saleszone' ),
        'singular_name' => __( 'kategorija pravljice', 'saleszone' ),
        'menu_name'     => __( 'kategorija pravljice', 'saleszone' ),
        'all_items'     => __( 'All kategorija pravljice', 'saleszone' ),
        'edit_item'     => __( 'Change kategorija pravljice', 'saleszone' ),
        'view_item'     => __( 'View kategorija pravljice', 'saleszone' ),
        'update_item'   => __( 'Update kategorija pravljice', 'saleszone' ),
        'add_new_item'  => __( 'Add a new kategorija pravljice to the gallery', 'saleszone' ),
        'search_items'  => __( 'Search kategorija pravljice', 'saleszone' ),
        'not_found'     => __( 'No kategorija pravljice found.', 'saleszone' ),
      ),
      'hierarchical'      => true,
      'show_in_rest' =>true,
      'rewrite' => array(
        'slug' => 'kategorija-pravljice'
      ),
    )
  );
  register_taxonomy(
    'zbirke-pravljice',
    'pravljice',
    array(
      'labels'       => array(
        'name'          => __( 'zbirka pravljice', 'saleszone' ),
        'singular_name' => __( 'zbirka pravljice', 'saleszone' ),
        'menu_name'     => __( 'zbirka pravljice', 'saleszone' ),
        'all_items'     => __( 'All zbirka pravljice', 'saleszone' ),
        'edit_item'     => __( 'Change zbirka pravljice', 'saleszone' ),
        'view_item'     => __( 'View zbirka pravljice', 'saleszone' ),
        'update_item'   => __( 'Update zbirka pravljice', 'saleszone' ),
        'add_new_item'  => __( 'Add a new zbirka pravljice to the gallery', 'saleszone' ),
        'search_items'  => __( 'Search zbirka pravljice', 'saleszone' ),
        'not_found'     => __( 'No zbirka pravljice found.', 'saleszone' ),
      ),
      'hierarchical'      => true,
      'show_in_rest' =>true,
      'rewrite' => array(
        'slug' => 'zbirke-pravljice'
      ),
    )
  );
}

add_action( 'init', 'themeprefix_register_post_types' );
