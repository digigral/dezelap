<?php
/**
* Setup Timber environment 
*/

function themeprefix_add_to_twig( $twig ) {
  
  $twig->addFilter( new Twig_SimpleFilter( 'merge_recursive', 'themeprefix_array_merge_recursive' ) );
  $twig->addFilter( new Twig_SimpleFilter( 'slugify', 'themeprefix_string_slugify' ) );
  $twig->addFunction( new Timber\Twig_Function('get_audio_duration', 'get_audio_duration_in_php'));
  $twig->addFunction( new Timber\Twig_Function('get_comon_pravljice', 'get_comon_pravljice_in_php'));
  $twig->addFunction( new Timber\Twig_Function('get_random_pravljice', 'get_random_pravljice_in_php'));
  $twig->addFunction( new Timber\Twig_Function('get_passage', 'get_passage_in_php'));

  return $twig;
}

add_filter( 'timber/twig', 'themeprefix_add_to_twig' );



function themeprefix_array_merge_recursive( $arr1, $arr2 ) {
  if ( $arr1 instanceof Traversable ) {
    $arr1 = iterator_to_array( $arr1 );
  } elseif ( !is_array( $arr1 ) ) {
    throw new Twig_Error_Runtime( sprintf( 'The merge_recursive filter only works with arrays or "Traversable", got "%s" as first argument.', gettype( $arr1 ) ) );
  }
  if ( $arr2 instanceof Traversable ) {
    $arr2 = iterator_to_array( $arr2 );
  } elseif ( !is_array( $arr2 ) ) {
    throw new Twig_Error_Runtime( sprintf( 'The merge_recursive filter only works with arrays or "Traversable", got "%s" as second argument.', gettype( $arr2 ) ) );
  }
  return array_replace_recursive( $arr1, $arr2 );
}



function themeprefix_string_slugify( $str ) {
  if ( gettype( $str ) === 'string' ) {
    $result = strtolower( str_replace( ' ', '-', $str ) );
  } else {
    throw new Twig_Error_Runtime( sprintf( 'The slugify filter only works with strings, got "%s" as first argument.', gettype( $str ) ) );
  }
  return $result;
}

function get_audio_duration_in_php($path){
  $pravljica = str_replace(get_site_url(), ABSPATH,$path);
  require_once( ABSPATH . "wp-content/themes/timber-boilerplate-master/vendor/autoload.php");
  $audio = new wapmorgan\Mp3Info\Mp3Info($pravljica); 
  $min = (floor($audio->duration / 60)) < 10 ? '0' . (floor($audio->duration / 60)) : (floor($audio->duration / 60));
  $sec = (floor($audio->duration % 60)) < 10 ? '0' . floor($audio->duration % 60) : (floor($audio->duration % 60));
  return $min .':'. $sec .PHP_EOL; 
}

function get_comon_pravljice_in_php($i_c = -1){
  $args = array(
    'post_type' => 'pravljice',
    'post_status' => 'publish',
    'posts_per_page' =>  $i_c,
  );
  return Timber::get_posts( $args );
}

function get_random_pravljice_in_php($i_c = -1){
  $args = array(
    'post_type' => 'pravljice',
    'post_status' => 'publish',
    'posts_per_page' =>  $i_c,
    'orderby'        => 'rand',
  );
  return Timber::get_posts( $args );
}



/**
 * Implements hook_preprocess_page()
 */
function get_passage_in_php($vari) {
  if(isset($_GET[$vari])){
    return explode(',',$_GET[$vari]);
  }
  else{
    return [];
  }

}