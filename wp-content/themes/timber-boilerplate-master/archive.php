<?php
/**
 * Singular pages (pages and posts of any type)
 */

use Timber\Timber;

$context = Timber::get_context();
$templates = array( 'category.twig', 'archive.twig');

$context['pagination'] = Timber::get_pagination();

Timber::render( $templates, $context );

