'use strict';
import { addBrowserClasses } from './modules/_utils';
import { example_module } from './modules/example-module';
// import { accordion } from './modules/accordion';

document.addEventListener('DOMContentLoaded', function () {

  document.getElementsByClassName('is-page-loading')[0].classList.remove('is-page-loading');
  document.getElementsByClassName('no-js')[0].classList.remove('no-js');

  // Common functions
  addBrowserClasses();

  // Import Javascript modules here:
  example_module();
  //accordion();
  "use strict";
  const options = {
    defaultSpeed: '1.00',
    speeds: ['1.25', '1.50', '2.00', '0.75'],
    loop: true,
    skipBackInterval: 15,
    jumpForwardInterval: 15,
    features: [
      "playpause",
      "progress",
      "current",
      "duration",
      "skipback",
      "changespeed",
      "volume",
      "jumpforward",
    ]
  }
  if (typeof MediaElementPlayer == "function") {
    new MediaElementPlayer(
      document.querySelector("audio"),
      options
    );
  }


  // Separate the audio controls so I can style them better.
  (() => {
    const elementTop = document.createElement('div');
    const elementBottom = document.createElement('div');

    elementTop.classList.add('mejs-prepended-buttons');
    elementBottom.classList.add('mejs-appended-buttons');

    const controls = document.querySelector('.mejs__controls');
    if (controls) {
      controls.prepend(elementTop);
      controls.append(elementBottom);
      const controlsChildren = Array.from(controls.childNodes).filter(v => v.className.startsWith("mejs_"));

      controlsChildren.slice(0, 3).forEach(elem => {
        elementTop.append(elem)
      });

      controlsChildren.slice(3, controlsChildren.length).forEach(elem => {
        elementBottom.append(elem)
      })
    }
  })()
  if (jQuery.fn.slick) {
    jQuery('.pravljice-podobne__list').slick({
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 3,
      rows: 2,
      arrows: false,
      responsive: [
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
      ]
    });
  }

  jQuery(function ($) {
    // Custom carousel nav
    $('.carousel-prev').click(function () {
      console.log('test');
      $('.pravljice-podobne__list').slick('slickPrev');
    });
    $('.carousel-next').click(function (e) {
      e.preventDefault();
      $('.pravljice-podobne__list').slick('slickNext');
    });
    $('.filter-menu__item-list').click(function (e) {
      let link = '';
      $('.filter-menu__item-list').each(function () {
        const tax = $(this).data('tax')
        let values = '';
        $(this).find('.filter-menu__item-item input').each(function () {
          if ($(this).prop("checked")) {
            if (values == '') {
              values = $(this).data('term');
            } else {
              values += ',' + $(this).data('term');
            }
          }
        });

        if (values) {
          if (link == '') {
            link = '?' + tax + '=' + values;
          } else {
            link += '&' + tax + '=' + values;
          }
        }
      });
      window.location.href = window.location.origin + window.location.pathname + link;
    });
    // SEARCH BUTTON OPEN/CLOSE OVERLAY
    $('.search-button').click(function (event) {
      event.preventDefault();
      $('#searchOverlay').css("display", "block");
      $('#page, section').css("filter", "blur(5px)");
    })

    $('.closebtn').click(function () {
      $('#searchOverlay').css("display", "none");
      $('#page, section').css("filter", "none");
    })

    $('.nav-hamburger').on('click', function () {
      if ($(this).hasClass('active')) {
        $(this).removeClass('active')
        $('.nav-menu-mobile').css('display', 'none');
      } else {
        $(this).addClass('active')
        $('.nav-menu-mobile').css('display', 'flex');
      }
    });

    $('.filter-menu-mobile').on('click', function () {
      if ($(this).hasClass('active')) {
        $(this).removeClass('active')
        $('.filter').css('display', 'none');
      } else {
        $(this).addClass('active')
        $('.filter').css('display', 'block');
      }
    });
  });
});
